#ifndef _MAKER_HUMIDITY_
#define _MAKER_HUMIDITY_

/*
                   _________ 
         _______  /         \__
     ___/       \/             \__
    /                             \___
   |                                  \
    \_                              __/ 
	  \___                      ___/
          \_____________/\_____/
      |         |      |
            |             |     |
         |         |                |
		       |        |    |
       |      |       |           |
	      |        |   |     |		  
		  
*/
	

/* Function shows analog humidity value of sensor */
uint8_t maker_returnHumidityValue(uint8_t analogPin);

/* Function shows Average humidity value of 10 mesurments */
double maker_returnHumidityAvg(uint8_t analogPin);

#endif
