/*
 * Library: maker_humidity.h
 *
 * Organization: MakerRobotics
 * Autors: Milan Gigic
 *
 * Date: 30.11.2017.
 * Test: Arduino UNO
 *
 * Note:
 * 
 */
 
#include "Arduino.h"
#include "maker_humidity.h"
#include "stdint.h"


/*Return analog sensor value*/
uint8_t maker_returnHumidityValue(uint8_t analogPin)
{
	return analogRead(analogPin);
}

/* Returns average value of 10 mesurments*/
double maker_returnHumidityAvg(uint8_t analogPin)
{
	double value;
	for (int i=0;i<10;i++)
	{
		value += maker_returnHumidityValue(analogPin);
	}
	value /= 10;
	return value;
}

